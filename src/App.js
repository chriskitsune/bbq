import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
  Button,
  Columns,
  Container,
  Form,
  Heading,
  Notification,
  Section,
} from 'react-bulma-components';
import BuddyList from './BuddyList';

class App extends Component {
  constructor() {
    super();
    this.state = {
      isFetching: false,
      error: null,
      buddies: [],
      addingBuddy: false,
      buddyName: '',
      buddyItems: [
        {
          name: '',
          quant: '',
        },
      ],
    };
    this.showError = this.showError.bind(this);
    this.getBuddies = this.getBuddies.bind(this);
    this.newBuddy = this.newBuddy.bind(this);
    this.buddyList = this.buddyList.bind(this);
    this.itemFields = this.itemFields.bind(this);
    this.newItem = this.newItem.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleItemChange = this.handleItemChange.bind(this);
    this.insertBuddy = this.insertBuddy.bind(this);
  }

  componentDidMount() {
    this.getBuddies();
  }

  getBuddies() {
    this.setState({
      isFetching: true,
    });
    fetch('https://us-central1-it-s-fridaaaaaay.cloudfunctions.net/buddies')
      .then((response) => response.json())
      .then((result) => {
        this.setState({
          buddies: result.buddies,
          isFetching: false,
        });
      })
      .catch((e) => {
        this.setState({
          error: e.message,
          isFetching: false,
        });
      });
  }

  newBuddy() {
    this.setState({
      addingBuddy: true,
    });
  }

  buddyList() {
    this.setState({
      addingBuddy: false,
      error: null,
    });
  }

  insertBuddy(e) {
    e.preventDefault();

    const { buddyName, buddyItems } = this.state;

    this.setState({
      isFetching: true,
      error: null,
    });

    fetch('https://us-central1-it-s-fridaaaaaay.cloudfunctions.net/buddy', {
      method: 'post',
      body: JSON.stringify({
        name: buddyName,
        items: buddyItems,
      }),
    })
      .then((response) => {
        this.setState({
          isFetching: false,
          error: null,
        });

        // JSON format on error response only
        if (response.ok) {
          this.setState({
            buddyName: '',
            buddyItems: [],
          });
          this.buddyList();
          this.getBuddies();
          return response;
        }
        return response.json();
      })
      .then((result) => {
        if (result.errors) {
          throw Error(result.message);
        }
      })
      .catch((error) => {
        this.setState({
          error: error.message,
          isFetching: false,
        });
      });
  }

  handleNameChange(e) {
    const { value } = e.target;
    this.setState({ buddyName: value });
  }

  handleItemChange(i, e) {
    const { name, value } = e.target;
    const buddyItems = [...this.state.buddyItems];
    buddyItems[i] = { ...buddyItems[i], [name]: value };
    this.setState({ buddyItems });
  }

  newItem() {
    this.setState((prevState) => ({
      buddyItems: [
        ...prevState.buddyItems,
        {
          name: '',
          quant: '',
        },
      ],
    }));
  }

  removeItem(i) {
    const buddyItems = [...this.state.buddyItems];
    buddyItems.splice(i, 1);
    this.setState({ buddyItems });
  }

  itemFields() {
    return this.state.buddyItems.map((el, i) => (
      <Columns key={i}>
        <Columns.Column size={6}>
          <Form.Input
            name="name"
            value={el.name || ''}
            type="text"
            placeholder="Item"
            onChange={(e) => this.handleItemChange(i, e)}
          />
        </Columns.Column>
        <Columns.Column size={5}>
          <Form.Input
            name="quant"
            value={el.quant || ''}
            type="text"
            placeholder="Item quantity"
            onChange={(e) => this.handleItemChange(i, e)}
          />
        </Columns.Column>
        <Columns.Column size={1}>
          {i !== 0
            ? (
              <Button type="button" onClick={(e) => this.removeItem(i, e)}>
                <span role="img" aria-label="Remove">
                  ❌
                </span>
              </Button>
            ) : '' }
        </Columns.Column>
      </Columns>
    ));
  }

  showError() {
    const { error } = this.state;
    return error ? (
      <Columns>
        <Columns.Column size={12}>
          <Notification color="danger">{error}</Notification>
        </Columns.Column>
      </Columns>
    ) : (
      ''
    );
  }

  renderForm() {
    const { buddyName, isFetching } = this.state;
    return (
      <>
        <Columns>
          <Columns.Column size={12}>
            <Heading size={3}>New BBQ Buddy</Heading>
          </Columns.Column>
        </Columns>
        {this.showError()}
        <form onSubmit={this.insertBuddy}>
          <Columns>
            <Columns.Column size={6}>
              <Form.Control style={{ marginBottom: 20 }}>
                <Form.Label>Buddy's Name</Form.Label>
                <Form.Input
                  name="name"
                  type="text"
                  placeholder="Jeremy"
                  value={buddyName}
                  onChange={this.handleNameChange}
                />
              </Form.Control>
            </Columns.Column>
            <Columns.Column size={6}>
              <Form.Control style={{ marginBottom: 20 }}>
                <Form.Label>Buddy's Items</Form.Label>
                {this.itemFields()}
                <Button type="button" color="link" onClick={this.newItem}>
                  Add Another Item
                </Button>
              </Form.Control>
            </Columns.Column>
            <Columns.Column size={12}>
              <Button style={{ marginRight: 10 }} onClick={this.buddyList}>
                Cancel
              </Button>
              <Button type="submit" color="primary" className={isFetching ? 'is-loading' : ''}>
                Add Buddy
              </Button>
            </Columns.Column>
          </Columns>
        </form>
      </>
    );
  }

  renderList() {
    const { buddies, isFetching } = this.state;
    return (
      <>
        <Columns>
          <Columns.Column size={6}>
            <Heading size={3}>BBQ Buddies</Heading>
          </Columns.Column>
          <Columns.Column size={6}>
            <Button
              style={{ float: 'right' }}
              color="primary"
              onClick={this.newBuddy}
            >
              New Buddy
            </Button>
          </Columns.Column>
        </Columns>
        {this.showError()}
        {isFetching ? (
          <Button className="is-loading is-large is-fullwidth" />
        ) : (
          <BuddyList buddies={buddies} />
        )}
      </>
    );
  }

  render() {
    const { addingBuddy } = this.state;
    return (
      <Section>
        <Container breakpoint="desktop">
          {addingBuddy ? this.renderForm() : this.renderList()}
        </Container>
      </Section>
    );
  }
}

export default App;
