import React from 'react';
import {
  Card, Heading, Image, Media,
} from 'react-bulma-components';

function Buddy(props) {
  const { name, items, avatar } = props;

  return (
    <Card>
      <Card.Content>
        <Media>
          <Media.Item renderAs="figure" position="left">
            <Image src={avatar} size={64} alt={name} />
          </Media.Item>
          <Media.Item>
            <Heading size={4}>{name}</Heading>
            <Heading subtitle size={6}>
              {items.map((item, i) => (
                <div key={i}>
                  <span>
                    {item.name}
                    {' '}
(
                    {item.quant}
)
                  </span>
                </div>
              ))}
            </Heading>
          </Media.Item>
        </Media>
      </Card.Content>
    </Card>
  );
}

export default Buddy;
