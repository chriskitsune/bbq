import React from 'react';
import { Columns } from 'react-bulma-components';
import Buddy from './Buddy';

function BuddyList(props) {
  const buddies = props.buddies.map((buddy, i) => (
    <Columns.Column key={i} size={4}>
      <Buddy
        key={buddy.id}
        name={buddy.name}
        items={buddy.items}
        avatar={buddy.avatar}
      />
    </Columns.Column>
  ));

  return <Columns>{buddies}</Columns>;
}

export default BuddyList;
